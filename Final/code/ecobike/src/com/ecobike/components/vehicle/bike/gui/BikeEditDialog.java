package com.ecobike.components.vehicle.bike.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.vehicle.gui.VehicleEditDialog;

@SuppressWarnings("serial")
public class BikeEditDialog extends VehicleEditDialog{
	
	private JTextField batteryPercentageField;
	private JTextField loadCyclesField;
	private JTextField estimatedUsageTimeRemainingField;
	
	public BikeEditDialog(Vehicle vehicle, IDataManageController<Vehicle> controller) {
		super(vehicle, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
//		if (t instanceof EBike) {
//			EBike bike = (EBike) t;
//			
//			int row = getLastRowIndex();
//			JLabel publisherLabel = new JLabel("Số pin:");
//			c.gridx = 0;
//			c.gridy = row;
//			getContentPane().add(publisherLabel, c);
//			batteryPercentageField = new JTextField(15);
//			batteryPercentageField.setText(bike.getBatteryPercentage() + "");
//			c.gridx = 1;
//			c.gridy = row;
//			getContentPane().add(batteryPercentageField, c);
//			
//			row = getLastRowIndex();
//			JLabel languageLabel = new JLabel("Số lần sạc");
//			c.gridx = 0;
//			c.gridy = row;
//			getContentPane().add(languageLabel, c);
//			loadCyclesField = new JTextField(15);
//			loadCyclesField.setText(bike.getLoadCycles() + "");
//			c.gridx = 1;
//			c.gridy = row;
//			getContentPane().add(loadCyclesField, c);
//			
//			row = getLastRowIndex();
//			JLabel estimatedUsageTimeRemainingLabel = new JLabel("Thời gian dự kiến");
//			c.gridx = 0;
//			c.gridy = row;
//			getContentPane().add(estimatedUsageTimeRemainingLabel, c);
//			estimatedUsageTimeRemainingField = new JTextField(15);
//			estimatedUsageTimeRemainingField.setText(bike.getLoadCycles() + "");
//			c.gridx = 1;
//			c.gridy = row;
//			getContentPane().add(estimatedUsageTimeRemainingField, c);
//		}
	}

	@Override
	public Vehicle getNewData() {
		super.getNewData();
		
		if (t instanceof Bike) {
			Bike bike = ( Bike) t;
			
//			bike.setBatteryPercentage(Integer.parseInt(batteryPercentageField.getText()));
//			bike.setLoadCycles(Integer.parseInt(loadCyclesField.getText()));
//			bike.setTimeRemaining(Integer.parseInt(estimatedUsageTimeRemainingField.getText()));
//			bike.setType("bike");
		}
		
		return t;
	}
}
