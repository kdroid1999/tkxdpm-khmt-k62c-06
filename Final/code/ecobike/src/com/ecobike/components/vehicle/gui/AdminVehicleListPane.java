package com.ecobike.components.vehicle.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ecobike.bean.Vehicle;
import com.ecobike.bean.EBike;
import com.ecobike.bean.Bike;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.abstractdata.gui.ADataSinglePane;
import com.ecobike.components.vehicle.bike.gui.BikeEditDialog;
import com.ecobike.components.vehicle.bike.gui.BikeCreateDialog;
import com.ecobike.components.vehicle.controller.AdminVehiclePageController;

@SuppressWarnings("serial")
public class AdminVehicleListPane extends ADataListPane<Vehicle>{
	
	public AdminVehicleListPane(ADataPageController<Vehicle> controller) {
		this.controller = controller;
	}
	
	@Override 
	public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) {
		JButton button = new JButton("Sửa");
		JButton createButton = new JButton("Thêm");
		singlePane.addDataHandlingComponent(button);
		singlePane.addDataHandlingComponent(createButton);
		
		IDataManageController<Vehicle> manageController = new IDataManageController<Vehicle>() {
			@Override
			public void update(Vehicle t) {
				if (controller instanceof AdminVehiclePageController) {
					Vehicle newVehicle = ((AdminVehiclePageController) controller).updateVehicle(t);
					singlePane.updateData(newVehicle);
				}
			}

			@Override
			public void create(Vehicle t) {
				if (controller instanceof AdminVehiclePageController) {
					((AdminVehiclePageController) controller).createVehicle(t);
				}
			}

			@Override
			public void read(Vehicle t) {
			}

			@Override
			public void delete(Vehicle t) {
				
			}
		};
		
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new BikeEditDialog(singlePane.getData(), manageController);
			}
		});	
		
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new BikeCreateDialog(singlePane.getData(), manageController);
			}
		});
	}
}
