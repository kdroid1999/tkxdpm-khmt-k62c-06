package com.ecobike.components.vehicle.controller;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.vehicle.gui.AdminVehicleListPane;

public abstract class AdminVehiclePageController extends ADataPageController<Vehicle> {
	public AdminVehiclePageController() {
		super();
	}
	
	@Override
	public ADataListPane<Vehicle> createListPane() {
		return new AdminVehicleListPane(this);
	}
	
	public abstract Vehicle updateVehicle(Vehicle Vehicle);
	
	public abstract void createVehicle(Vehicle Vehicle);
	
}
