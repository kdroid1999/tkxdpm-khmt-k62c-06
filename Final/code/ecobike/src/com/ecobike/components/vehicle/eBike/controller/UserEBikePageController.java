package com.ecobike.components.vehicle.eBike.controller;

import java.util.List;
import java.util.Map;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.vehicle.bike.gui.BikeSearchPane;
import com.ecobike.components.vehicle.bike.gui.BikeSinglePane;
import com.ecobike.components.vehicle.controller.UserVehiclePageController;
import com.ecobike.components.vehicle.gui.VehicleSearchPane;
import com.ecobike.components.vehicle.gui.VehicleSinglePane;
import com.ecobike.serverapi.VehicleApi;

public class UserEBikePageController extends UserVehiclePageController{
	public UserEBikePageController() {
		super();
	}
//	public UserBookPageController(CartController cartController) {
//		super(cartController);
//	}
	@Override
	public List<? extends Vehicle> search(Map<String, String> searchParams) {
		return new VehicleApi().getEBikes(searchParams);
	}
	@Override
	public VehicleSinglePane createSinglePane() {
		return new BikeSinglePane();
	}
	@Override
	public VehicleSearchPane createSearchPane() {
		return new BikeSearchPane();
	}
}
