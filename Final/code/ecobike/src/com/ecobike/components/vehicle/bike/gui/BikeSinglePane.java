package com.ecobike.components.vehicle.bike.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.vehicle.gui.VehicleSinglePane;

@SuppressWarnings("serial")
public class BikeSinglePane extends VehicleSinglePane {	
	private JLabel labelBatteryPercentage;
	private JLabel labelLoadCycles;
	private JLabel labelEstimatedUsageTimeRemaining;
	
	public BikeSinglePane() {
		super();
	}
	
	public BikeSinglePane(Vehicle vehicle) {
		this();
		this.t = vehicle;

		displayData();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();

		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBatteryPercentage = new JLabel();
		add(labelBatteryPercentage, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLoadCycles = new JLabel();
		add(labelLoadCycles, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelEstimatedUsageTimeRemaining = new JLabel();
		add(labelEstimatedUsageTimeRemaining, c);
	}
	
	
	@Override
	public void displayData() {
		super.displayData();
		
		if (t instanceof EBike) {
			EBike eBike = (EBike) t;
			
			labelBatteryPercentage.setText("Số pin: " + eBike.getBatteryPercentage());
			labelLoadCycles.setText("Số lần sạc: " + eBike.getLoadCycles());
			labelEstimatedUsageTimeRemaining.setText("Thời gian dự kiến: " + eBike.getTimeRemaining());
		}
	}
}
