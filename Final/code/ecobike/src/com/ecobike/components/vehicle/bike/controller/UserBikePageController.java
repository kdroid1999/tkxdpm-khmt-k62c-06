package com.ecobike.components.vehicle.bike.controller;

import java.util.List;
import java.util.Map;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.order.controller.OrderController;
import com.ecobike.components.vehicle.bike.gui.BikeSearchPane;
import com.ecobike.components.vehicle.bike.gui.BikeSinglePane;
import com.ecobike.components.vehicle.controller.UserVehiclePageController;
import com.ecobike.components.vehicle.gui.VehicleSearchPane;
import com.ecobike.components.vehicle.gui.VehicleSinglePane;
import com.ecobike.serverapi.VehicleApi;

public class UserBikePageController extends UserVehiclePageController{
	public UserBikePageController() {
		super();
	}
	public UserBikePageController(OrderController orderController) {
		super(orderController);
	}
	@Override
	public List<? extends Vehicle> search(Map<String, String> searchParams) {
		return new VehicleApi().getBikes(searchParams);
	}
	@Override
	public VehicleSinglePane createSinglePane() {
		return new BikeSinglePane();
	}
	@Override
	public VehicleSearchPane createSearchPane() {
		return new BikeSearchPane();
	}
}
