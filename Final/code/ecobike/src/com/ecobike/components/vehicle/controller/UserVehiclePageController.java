package com.ecobike.components.vehicle.controller;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.order.controller.OrderController;
import com.ecobike.components.vehicle.gui.UserVehicleListPane;

public abstract class UserVehiclePageController extends ADataPageController<Vehicle> {
	private OrderController orderController;
	
	public UserVehiclePageController() {
		super();
	}
	
	public UserVehiclePageController(OrderController orderController) {
		this();
		setOrderController(orderController);
	}
	
	public void setOrderController(OrderController orderController) {
		this.orderController = orderController;
	}
	
	@Override
	public ADataListPane<Vehicle> createListPane() {
		return new UserVehicleListPane(this);
	}
	
	public void addToOrder(int vehicleId, String vehicleName, float vehicleCost) {
		orderController.addToOrder(vehicleId, vehicleName, vehicleCost);
	}
}
