package com.ecobike.components.vehicle.bike.gui;

import javax.swing.JTextField;

import com.ecobike.bean.EBike;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.vehicle.gui.VehicleCreateDialog;

@SuppressWarnings("serial")
public class BikeCreateDialog extends VehicleCreateDialog{
	
	private JTextField batteryPercentageField;
	private JTextField loadCyclesField;
	private JTextField estimatedUsageTimeRemainingField;
	
	public BikeCreateDialog(Vehicle vehicle, IDataManageController<Vehicle> controller) {
		super(vehicle, controller);
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
	}

	@Override
	public Vehicle getNewData() {
		super.getNewData();
		System.out.println(t);
		
		if (t instanceof EBike) {
			EBike bike = (EBike) t;
			
			bike.setBatteryPercentage(Integer.parseInt(batteryPercentageField.getText()));
			bike.setLoadCycles(Integer.parseInt(loadCyclesField.getText()));
			bike.setTimeRemaining(Integer.parseInt(estimatedUsageTimeRemainingField.getText()));
		}
		
		return t;
	}
}
