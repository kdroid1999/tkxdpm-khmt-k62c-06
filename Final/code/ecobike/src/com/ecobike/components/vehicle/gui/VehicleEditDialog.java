package com.ecobike.components.vehicle.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.abstractdata.gui.ADataEditDialog;

@SuppressWarnings("serial")
public class VehicleEditDialog extends ADataEditDialog<Vehicle>{
	
	private JTextField nameField;
//	private JTextField typeField;
	private JTextField weightField;
	private JTextField licensePlateField;
	private JTextField manuafacturingDateField;
	private JTextField producerField;
	private JTextField costField;
	
	public VehicleEditDialog(Vehicle vehicle, IDataManageController<Vehicle> controller) {
		super(vehicle, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Tên: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);	
		
//		row = getLastRowIndex();
//		JLabel typeLabel = new JLabel("Thể loại: ");
//		c.gridx = 0;
//		c.gridy = row;
//		getContentPane().add(typeLabel, c);
//		typeField = new JTextField(15);
//		typeField.setText(t.getType());
//		c.gridx = 1;
//		c.gridy = row;
//		getContentPane().add(typeField, c);
		
		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Trọng lượng: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		weightField.setText(t.getWeight() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(weightField, c);
		
		row = getLastRowIndex();
		JLabel licensePlateLabel = new JLabel("Biển số: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(licensePlateLabel, c);
		licensePlateField = new JTextField(15);
		licensePlateField.setText(t.getLicensePlate());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(licensePlateField, c);
		
		row = getLastRowIndex();
		JLabel manuafacturingDateLabel = new JLabel("Ngày sản xuất: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(manuafacturingDateLabel, c);
		manuafacturingDateField = new JTextField(15);
		manuafacturingDateField.setText(t.getManuafacturingDate());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(manuafacturingDateField, c);
		
		row = getLastRowIndex();
		JLabel producerLabel = new JLabel("Hãng sản xuất: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(producerLabel, c);
		producerField = new JTextField(15);
		producerField.setText(t.getProducer());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(producerField, c);
		
		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Giá: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		costField.setText(t.getCost() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(costField, c);
	}

	@Override
	public Vehicle getNewData() {
		t.setId_bike(t.getId_bike());
		t.setName(nameField.getText());
//		t.setType(typeField.getText());
		t.setWeight(Float.parseFloat(weightField.getText()));
		t.setLicensePlate(licensePlateField.getText());
		t.setManuafacturingDate(manuafacturingDateField.getText());
		t.setProducer(producerField.getText());
		t.setCost(Float.parseFloat(costField.getText()));
		
		return t;
	}
}
