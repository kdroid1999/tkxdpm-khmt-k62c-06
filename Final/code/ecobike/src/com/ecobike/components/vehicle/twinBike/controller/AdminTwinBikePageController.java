package com.ecobike.components.vehicle.twinBike.controller;

import java.util.List;
import java.util.Map;

import com.ecobike.bean.Bike;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.vehicle.bike.gui.BikeSearchPane;
import com.ecobike.components.vehicle.bike.gui.BikeSinglePane;
import com.ecobike.components.vehicle.controller.AdminVehiclePageController;
import com.ecobike.components.vehicle.gui.VehicleSearchPane;
import com.ecobike.components.vehicle.gui.VehicleSinglePane;
import com.ecobike.serverapi.VehicleApi;

public class AdminTwinBikePageController extends AdminVehiclePageController{
	@Override
	public List<? extends Vehicle> search(Map<String, String> searchParams) {
		return new VehicleApi().getTwinBikes(searchParams);
	}
	
	@Override
	public VehicleSinglePane createSinglePane() {
		return new BikeSinglePane();
	}
	
	@Override
	public VehicleSearchPane createSearchPane() {
		return new BikeSearchPane();
	}
	
	@Override
	public Vehicle updateVehicle(Vehicle vehicle) {
		return new VehicleApi().updateBike((Bike) vehicle);
	}
	
	@Override
	public void createVehicle(Vehicle vehicle) {
		new VehicleApi().createVehicle(vehicle);
	}
}
