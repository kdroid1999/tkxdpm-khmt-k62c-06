package com.ecobike.components.vehicle.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.abstractdata.gui.ADataSinglePane;
import com.ecobike.components.vehicle.controller.UserVehiclePageController;

@SuppressWarnings("serial")
public class UserVehicleListPane extends ADataListPane<Vehicle>{
	
	public UserVehicleListPane(ADataPageController<Vehicle> controller) {
		this.controller = controller;
	} 

	@Override
	public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) { 
		
		JButton button = new JButton("Thuê");
		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (controller instanceof UserVehiclePageController) {
					((UserVehiclePageController) controller).addToOrder(singlePane.getData().getId_bike(), singlePane.getData().getName(),
							singlePane.getData().getCost());
				}
			}
		});
	}
}
