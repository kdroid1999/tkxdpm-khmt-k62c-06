package com.ecobike.components.vehicle.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.abstractdata.gui.ADataCreateDialog;

@SuppressWarnings("serial")
public class VehicleCreateDialog extends ADataCreateDialog<Vehicle>{
	
	private JTextField nameField;
    private JComboBox typeField;
    private JTextField weightField;
    private JTextField licensePlateField;
    private JTextField manuafacturingDateField;
    private JTextField producerField;
    private JTextField costField;
    private JTextField idStationField;
    private JTextField batteryPercentField;
    private JTextField loadCycleField;
    private JTextField estimateTimeField;
	
	public VehicleCreateDialog(Vehicle vehicle, IDataManageController<Vehicle> controller) {
		super(vehicle, controller);
	} 

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
        JLabel nameLabel = new JLabel("Tên xe");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(nameLabel, c);
        nameField = new JTextField(15);
        nameField.setPreferredSize(new Dimension(168, 30));
        nameField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(nameField, c);

        row = getLastRowIndex();
        JLabel typeLabel = new JLabel("Loại xe");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(typeLabel, c);
        String typeList[] = { "bike", "ebike", "twinbike" };
        typeField = new JComboBox(typeList);
        int typeWidth = 126; // 153 nếu lỗi
        typeField.setPreferredSize(new Dimension(typeWidth, 30));
        typeField.setMinimumSize(new Dimension(typeWidth, 30));
        typeField.setMaximumSize(new Dimension(typeWidth, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(typeField, c);

        row = getLastRowIndex();
        JLabel weightLabel = new JLabel("Khối lượng(kg)");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(weightLabel, c);
        weightField = new JTextField(15);
        weightField.setPreferredSize(new Dimension(168, 30));
        weightField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(weightField, c);

        row = getLastRowIndex();
        JLabel licensePlateLabel = new JLabel("Biển số xe");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(licensePlateLabel, c);
        licensePlateField = new JTextField(15);
        licensePlateField.setPreferredSize(new Dimension(168, 30));
        licensePlateField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(licensePlateField, c);

        row = getLastRowIndex();
        JLabel manufactoringDateLabel = new JLabel("Ngày sản xuất");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(manufactoringDateLabel, c);
        manuafacturingDateField = new JTextField(15);
        manuafacturingDateField.setPreferredSize(new Dimension(168, 30));
        manuafacturingDateField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(manuafacturingDateField, c);

        row = getLastRowIndex();
        JLabel producerLabel = new JLabel("Nhà sản xuất");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(producerLabel, c);
        producerField = new JTextField(15);
        producerField.setPreferredSize(new Dimension(168, 30));
        producerField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(producerField, c);

        row = getLastRowIndex();
        JLabel costLabel = new JLabel("Giá thành(VNĐ)");
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(costLabel, c);
        costField = new JTextField(15);
        costField.setPreferredSize(new Dimension(168, 30));
        costField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        getContentPane().add(costField, c);

        row = getLastRowIndex();
        JLabel batteryPercentLabel = new JLabel("Dung lượng pin hiện tại(%)");
        batteryPercentLabel.setVisible(false);
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(batteryPercentLabel, c);
        batteryPercentField = new JTextField(15);
        batteryPercentField.setPreferredSize(new Dimension(168, 30));
        batteryPercentField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        batteryPercentField.setVisible(false);
        getContentPane().add(batteryPercentField, c);

        row = getLastRowIndex() + 1;
        JLabel loadCycleLabel = new JLabel("Số lần sạc");
        loadCycleLabel.setVisible(false);
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(loadCycleLabel, c);
        loadCycleField = new JTextField(15);
        loadCycleField.setPreferredSize(new Dimension(168, 30));
        loadCycleField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        loadCycleField.setVisible(false);
        getContentPane().add(loadCycleField, c);

        row = getLastRowIndex() + 2;
        JLabel estimateTimeLabel = new JLabel("Thời gian sử dụng còn lại(phút)");
        estimateTimeLabel.setVisible(false);
        c.gridx = 0;
        c.gridy = row;
        getContentPane().add(estimateTimeLabel, c);
        estimateTimeField = new JTextField(15);
        estimateTimeField.setPreferredSize(new Dimension(168, 30));
        estimateTimeField.setMinimumSize(new Dimension(168, 30));
        c.gridx = 1;
        c.gridy = row;
        estimateTimeField.setVisible(false);
        getContentPane().add(estimateTimeField, c);

        typeField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (typeField.getSelectedItem() == "ebike") {
                    batteryPercentLabel.setVisible(true);
                    batteryPercentField.setVisible(true);
                    loadCycleLabel.setVisible(true);
                    loadCycleField.setVisible(true);
                    estimateTimeLabel.setVisible(true);
                    estimateTimeField.setVisible(true);
                    SwingUtilities.getRoot(typeField).setSize(300, 400);
                } else {
                    batteryPercentLabel.setVisible(false);
                    batteryPercentField.setVisible(false);
                    loadCycleLabel.setVisible(false);
                    loadCycleField.setVisible(false);
                    estimateTimeLabel.setVisible(false);
                    estimateTimeField.setVisible(false);
                    SwingUtilities.getRoot(typeField).setSize(300, 300);
                }
            }
        });
        
	}
	@Override
	public Vehicle getNewData() {
		t.setName(nameField.getText());
		t.setType(typeField.getSelectedItem().toString()); 
		t.setWeight(Float.parseFloat(weightField.getText()));
		t.setLicensePlate(licensePlateField.getText());
		t.setManuafacturingDate(manuafacturingDateField.getText());
		t.setProducer(producerField.getText());
		t.setCost(Float.parseFloat(costField.getText()));
		t.setIdPark(1);
		JOptionPane.showMessageDialog(null, "Tạo xe mới thành công");
		return t;
	}
}
