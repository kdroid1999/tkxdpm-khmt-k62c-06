package com.ecobike.components.vehicle.gui;

import javax.swing.JLabel;

import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.gui.ADataSinglePane;

@SuppressWarnings("serial")
public class VehicleSinglePane extends ADataSinglePane<Vehicle>{
	private JLabel labelName;
//	private JLabel labelType;
	private JLabel labelWeight;
	private JLabel labelLisencePlate;
	private JLabel labelManufactureDate;
	private JLabel labelProducer;
	private JLabel labelCost;
	private JLabel labelIdPark;
	private JLabel labelBatteryPercentage;
	
	
	public VehicleSinglePane() {
		super();
	}
		
	
	public VehicleSinglePane(Vehicle vehicle) {
		this();
		this.t = vehicle;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
//		row = getLastRowIndex();
//		c.gridx = 0;
//		c.gridy = row;
//		labelType = new JLabel();
//		add(labelType, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLisencePlate = new JLabel();
		add(labelLisencePlate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelManufactureDate = new JLabel();
		add(labelManufactureDate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelProducer = new JLabel();
		add(labelProducer, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelIdPark = new JLabel();
		add(labelIdPark, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBatteryPercentage = new JLabel();
		add(labelBatteryPercentage, c);
		

	}
	
	
	@Override
	public void displayData() {
		labelName.setText("Tên: " + t.getName() + "");
//		labelType.setText("Loại: " + t.getType() + "");
//		labelType.setText("Loại: bike");
		labelWeight.setText("Khối lượng: " + t.getWeight() + "");
		labelLisencePlate.setText("Biển số xe: " + t.getLicensePlate() + "");
		labelManufactureDate.setText("Hạn sử dụng: " + t.getManuafacturingDate() + "");
		labelProducer.setText("Nhà sản xuất: " + t.getProducer() + "");
		labelCost.setText("Giá: " + t.getCost() + "");
		labelIdPark.setText("ID bãi xe: " + t.getIdPark() + "");

	}
}