package com.ecobike.components.order.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ecobike.components.order.controller.OrderController;

@SuppressWarnings("serial")
public class OrderPane extends JPanel{
	private JLabel cartStatusLabel;
	private OrderController controller;
	
	public OrderPane() {
		this.setLayout(new FlowLayout(FlowLayout.RIGHT));
		cartStatusLabel = new JLabel();
		this.add(cartStatusLabel);
		JButton detailButton = new JButton("Detail");
		this.add(detailButton);
		
		
		detailButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.showOrderDialog();
			}
		});
	}
	
	public void setController(OrderController controller) {
		this.controller = controller;
	}
	
	public void updateData(String text) {
		cartStatusLabel.setText(text);
	}
}
