package com.ecobike.components.order.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.ecobike.bean.Order;
import com.ecobike.components.order.controller.OrderController;

@SuppressWarnings("serial")
public class OrderDialog extends JDialog{
	private GridBagLayout layout;
	private GridBagConstraints c;
	
	private JTextField cardIdField;
	private JTextField bikeIdField;
	
	private OrderController controller;
	
	public OrderDialog() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		c = new GridBagConstraints();
		
		updateData(null);
	}
	
	public void setController(OrderController controller) {
		this.controller = controller;
	}
	
	public void updateData(Order order) {
		this.getContentPane().removeAll();

		c.insets = new Insets(10,0,5,0);
		c.gridx = 0;
		c.gridy = 0;
		add(new JLabel("Mã thẻ"), c);
		c.gridx = 1;
		c.gridy = 0;
		cardIdField = new JTextField(15);
		add(cardIdField, c);
		
		c.insets = new Insets(0,0,5,0);
		c.gridx = 0;
		c.gridy = 1;
		add(new JLabel("Mã xe"), c);
		c.gridx = 1;
		c.gridy = 1;
		bikeIdField = new JTextField(15);
		add(bikeIdField, c);

		cardIdField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				order.setCardId(Integer.parseInt(cardIdField.getText()));
			}
		});
		bikeIdField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				order.setBikeId(Integer.parseInt(bikeIdField.getText()));
			}
		});
		
		
		
	// 	if (order != null) {
	// 		for (int i=0; i<order.getOrderLines().size(); ++i) {
	// 			OrderLine ol = order.getOrderLines().get(i);
				
	// 			int row = getLastRowIndex();
	// 			c.gridx = 0;
	// 			c.gridy = row;
	// 			JLabel label = new JLabel(ol.getProductTitle());
	// 			add(label, c);
				
	// 			c.gridx = 1;
	// 			c.gridy = row;
	// 			JSpinner spin = new JSpinner();
	// 			spin.setModel(new SpinnerNumberModel(ol.getProductQuantity(), 0, null, 1));
	// 			add(spin, c);
	// 			spin.setPreferredSize(new Dimension(190, 20));
				
	// 			spin.addChangeListener(new ChangeListener() {
	// 				@Override
	// 				public void stateChanged(ChangeEvent e) {
	// 					controller.setOrderLineQuantity(ol, (int) spin.getValue());
						
	// 					if ((int) spin.getValue() == 0) { // Remove item with 0 quantity
	// 						remove(spin);
	// 						remove(label);
	// 						pack();
	// 					}
	// 				}
	// 			});
	// 		}
			
	// 		int row = getLastRowIndex();
	// 		c.gridx = 0;
	// 		c.gridy = row;
	// 		JButton button = new JButton("Check out!");
	// 		add(button, c);
	// 		button.addActionListener(new ActionListener() {
	// 			@Override
	// 			public void actionPerformed(ActionEvent e) {
	// 				controller.checkOut();
	// 			}
	// 		});
	// 	}

	// 	this.revalidate();
	// 	this.repaint();
	// 	this.pack();
	// 	this.setResizable(false);
	// }
}
}