package com.ecobike.components.order.controller;
//import sau

import com.ecobike.bean.Order;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.order.gui.OrderDialog;
import com.ecobike.components.order.gui.OrderPane;

public class OrderController {
	private OrderPane orderPane;
	private OrderDialog dialog;
	private Order order;
	private Vehicle vehicle;
	
	public OrderController() {
		vehicle = new Vehicle();
		order = new Order();
		dialog = new OrderDialog();
		orderPane = new OrderPane();
		orderPane.setController(this);
		dialog.setController(this);		
		updateOrderPane(order.getCardId());
	}

	private void updateOrderPane(int cardId) {
		orderPane.updateData(getPresentationText());
	}
	
	private String getPresentationText() {
		return "Bạn đã thuê 1 xe. Chi phí thuê: " + order.getRealCost();
	}
	
	public OrderPane getOrderPane() {
		return orderPane;
	}
	
	public void showOrderDialog() {
		dialog.setVisible(true);
	}
	
	public void addToOrder(int vehicleId, String vehicleName, float vehicleCost) {
		order= new Order(vehicleId, vehicleName, vehicleCost);
		dialog.updateData(order);
		updateOrderPane(order.getCardId());
	}
	
	public void checkOut() {
		System.out.println("Chốt sale!!!!");
	}
}

