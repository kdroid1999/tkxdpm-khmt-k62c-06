package com.ecobike.components.station.controller;

import java.util.List;
import java.util.Map;

import com.ecobike.bean.Station;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.station.gui.AdminStationListPane;
import com.ecobike.components.station.gui.StationSearchPane;
import com.ecobike.components.station.gui.StationSinglePane;
import com.ecobike.serverapi.VehicleApi;

public class AdminStationPageController extends ADataPageController<Station> {
	public AdminStationPageController() {
		super();
	}
	
	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		return new VehicleApi().getStations(searchParams);
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new AdminStationListPane(this);
	}
	
	@Override
	public StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	public Station updateStation(Station station) {
		return new VehicleApi().updateStation((Station) station);
	}
	
	public Station createStation(Station station) {
		return new VehicleApi().createStation((Station) station);
	}
}
