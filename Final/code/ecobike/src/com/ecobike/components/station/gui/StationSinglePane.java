package com.ecobike.components.station.gui;

import javax.swing.JLabel;

import com.ecobike.bean.Station;
import com.ecobike.components.abstractdata.gui.ADataSinglePane;

@SuppressWarnings("serial")
public class StationSinglePane  extends ADataSinglePane<Station>{
	private JLabel labelStationName;
	private JLabel labelStationAddress;
	private JLabel labelNumberOfBikes;
	private JLabel labelNumberOfEBikes;
	private JLabel labelNumberOfTwinBikes;
	private JLabel labelNumberOfEmptyDocks;
	private JLabel labelAreaToWalk;
	private JLabel labelTimeToWalk;	
	
	public StationSinglePane() {
		super();
	}
		
	
	public StationSinglePane(Station station) {
		this();
		this.t = station;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationName = new JLabel();
		add(labelStationName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelStationAddress = new JLabel();
		add(labelStationAddress, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfBikes = new JLabel();
		add(labelNumberOfBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfEBikes = new JLabel();
		add(labelNumberOfEBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfTwinBikes = new JLabel();
		add(labelNumberOfTwinBikes, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfEmptyDocks = new JLabel();
		add(labelNumberOfEmptyDocks, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelAreaToWalk = new JLabel();
		add(labelAreaToWalk, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelTimeToWalk = new JLabel();
		add(labelTimeToWalk, c);

	}
	
	
	@Override
	public void displayData() {
		labelStationName.setText("Tên: " + t.getStationName() + "");
		labelStationAddress.setText("Địa chỉ: " + t.getStationAddress() + "");
		labelNumberOfBikes.setText("Số xe đạp: " + t.getNumberOfBikes() + "");
		labelNumberOfEBikes.setText("Số xe đạp điện: " + t.getNumberOfEBikes() + "");
		labelNumberOfTwinBikes.setText("Số xe đạp đôi: " + t.getNumberOfTwinBikes() + "");
		labelNumberOfEmptyDocks.setText("Số chỗ trống: " + t.getNumberOfEmptyDocks() + "");
		labelAreaToWalk.setText("Khoảng cách đi bộ: " + t.getAreaToWalk() + "");
		labelTimeToWalk.setText("Thời gian đi bộ ước tính: " + t.getTimeToWalk() + "");

	}
}
