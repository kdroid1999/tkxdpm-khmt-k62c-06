package com.ecobike.components.station.controller;


import java.util.List;
import java.util.Map;

import com.ecobike.bean.Station;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.order.controller.OrderController;
import com.ecobike.components.station.gui.StationSearchPane;
import com.ecobike.components.station.gui.StationSinglePane;
import com.ecobike.components.station.gui.UserStationListPane;
import com.ecobike.serverapi.VehicleApi;

// import GUI

// import com.oms.bean.Station;
// import com.oms.components.abstractdata.gui.ADataListPane;
// import com.oms.components.station.gui.UserStationListPane;

public class UserStationPageController extends ADataPageController<Station> {
	private OrderController orderController;
	
	public UserStationPageController() {
		super();
	}
	
	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		return new VehicleApi().getStations(searchParams);
	}
	
	public UserStationPageController(OrderController orderController) {
		this();
		setOrderController(orderController);
	}
	
	public void setOrderController(OrderController orderController) {
		this.orderController = orderController;
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new UserStationListPane(this);
	}
	
	@Override
	public StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	public List<Vehicle> getBikesInStation(Station station) {
		return new VehicleApi().getBikesInStation(station);
	}

//	
//	public void addToOrder(String vehicleId, String vehicleName, float vehicleCost) {
//		orderController.addToOrder(vehicleId, vehicleName, vehicleCost);
//	}
}

