package com.ecobike.components.station.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Station;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.abstractdata.gui.ADataCreateDialog;

@SuppressWarnings("serial")
public class StationCreateDialog extends ADataCreateDialog<Station> {

	private JTextField stationNameField;
	private JTextField stationAddressField;
	private JTextField numberOfBikesField;
	private JTextField numberOfEBikesField;
	private JTextField numberOfTwinBikesField;
	private JTextField numberOfEmptyDocksField;
	private JTextField areaToWalkField;
	private JTextField timeToWalkField;	
	
	public StationCreateDialog(Station station, IDataManageController<Station> controller) {
		super(station, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel stationNameLabel = new JLabel("Tên: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(stationNameLabel, c);
		stationNameField = new JTextField(15);
		stationNameField.setText(t.getStationName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(stationNameField, c);	
		
		row = getLastRowIndex();
		JLabel stationAddressLabel = new JLabel("Địa chỉ: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(stationAddressLabel, c);
		stationAddressField = new JTextField(15);
		stationAddressField.setText(t.getStationAddress());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(stationAddressField, c);
		
		row = getLastRowIndex();
		JLabel numberOfBikesLabel = new JLabel("Số xe đạp: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfBikesLabel, c);
		numberOfBikesField = new JTextField(15);
		numberOfBikesField.setText(t.getNumberOfBikes() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfBikesField, c);
		
		row = getLastRowIndex();
		JLabel numberOfEBikesLabel = new JLabel("Số xe đạp điện: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfEBikesLabel, c);
		numberOfEBikesField = new JTextField(15);
		numberOfEBikesField.setText(t.getNumberOfEBikes() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfEBikesField, c);
		
		row = getLastRowIndex();
		JLabel numberOfTwinBikesLabel = new JLabel("Số xe đạp đôi: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfTwinBikesLabel, c);
		numberOfTwinBikesField = new JTextField(15);
		numberOfTwinBikesField.setText(t.getNumberOfTwinBikes() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfTwinBikesField, c);
		
		row = getLastRowIndex();
		JLabel numberOfEmptyDocksLabel = new JLabel("Số chỗ trống: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numberOfEmptyDocksLabel, c);
		numberOfEmptyDocksField = new JTextField(15);
		numberOfEmptyDocksField.setText(t.getNumberOfEmptyDocks() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numberOfEmptyDocksField, c);
		
		row = getLastRowIndex();
		JLabel licensePlateLabel = new JLabel("Khoảng cách đi bộ: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(licensePlateLabel, c);
		areaToWalkField = new JTextField(15);
		areaToWalkField.setText(t.getAreaToWalk() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(areaToWalkField, c);
		
		row = getLastRowIndex();
		JLabel timeToWalkLabel = new JLabel("Thời gian đi bộ ước tính: ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(timeToWalkLabel, c);
		timeToWalkField = new JTextField(15);
		timeToWalkField.setText(t.getTimeToWalk() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(timeToWalkField, c);
		
	}

	@Override
	public Station getNewData() {
		t.setStationName(stationNameField.getText());
		t.setStationAddress(stationAddressField.getText());
		t.setNumberOfBikes(Integer.parseInt(numberOfBikesField.getText()));
		t.setNumberOfEBikes(Integer.parseInt(numberOfEBikesField.getText()));
		t.setNumberOfTwinBikes(Integer.parseInt(numberOfTwinBikesField.getText()));
		t.setNumberOfEmptyDocks(Integer.parseInt(numberOfEmptyDocksField.getText()));
		t.setAreaToWalk(Float.parseFloat(areaToWalkField.getText())); 
		t.setTimeToWalk(Integer.parseInt(timeToWalkField.getText()));
		
		return t;
	}

}
