package com.ecobike.components.station.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.ecobike.bean.Station;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.controller.IDataManageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.abstractdata.gui.ADataSinglePane;
import com.ecobike.components.station.gui.StationEditDialog;
import com.ecobike.components.vehicle.controller.AdminVehiclePageController;
import com.ecobike.components.station.controller.AdminStationPageController;

@SuppressWarnings("serial")
public class AdminStationListPane extends ADataListPane<Station>{
	
	public AdminStationListPane(ADataPageController<Station> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {
		JButton button = new JButton("Sửa"); 
		JButton buttonAddStation = new JButton("Thêm bãi xe"); 
		singlePane.addDataHandlingComponent(button);
		singlePane.addDataHandlingComponent(buttonAddStation);
		
		IDataManageController<Station> manageController = new IDataManageController<Station>() {
			@Override
			public void update(Station t) {
				if (controller instanceof AdminStationPageController) {
					Station newStation = ((AdminStationPageController) controller).updateStation(t);
					singlePane.updateData(newStation);
				}
			}

			@Override
			public void create(Station t) {
				if (controller instanceof AdminStationPageController) {
					((AdminStationPageController) controller).createStation(t);
				}
			}

			@Override
			public void read(Station t) {
			}

			@Override
			public void delete(Station t) {
				
			}
		};
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new StationEditDialog(singlePane.getData(), manageController);
			}
		});	
		
		buttonAddStation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Station s = new Station();
				new StationCreateDialog(s, manageController);
			}
		});	
	}
}
