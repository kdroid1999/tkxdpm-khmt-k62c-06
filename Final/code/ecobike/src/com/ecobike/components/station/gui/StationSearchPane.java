package com.ecobike.components.station.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.components.abstractdata.gui.ADataSearchPane;

@SuppressWarnings("serial")
public class StationSearchPane extends ADataSearchPane {
	private JTextField stationNameField;
	private JTextField stationAddressField;

	public StationSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel stationNameLabel = new JLabel("Tên:");
		stationNameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(stationNameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(stationNameField, c);
		
		JLabel stationAddressLabel = new JLabel("Địa chỉ:");
		stationAddressField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(stationAddressLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(stationAddressField, c);
		
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
//		if (!stationNameField.getText().trim().equals("")) {
//			res.put("stationName", stationNameField.getText().trim());
//		}
//		if (!stationAddressField.getText().trim().equals("")) {
////			System.out.println(stationAddressField.getText());
//			res.put("stationAddress", stationAddressField.getText().trim());
//		}
		res.put("stationName", stationNameField.getText().trim());
		res.put("stationAddress", stationAddressField.getText().trim());
		return res;
	}
}
