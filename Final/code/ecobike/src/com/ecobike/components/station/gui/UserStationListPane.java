package com.ecobike.components.station.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.ecobike.app.user.ECBUser;
import com.ecobike.bean.Station;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.abstractdata.gui.ADataListPane;
import com.ecobike.components.abstractdata.gui.ADataSinglePane;
import com.ecobike.components.station.controller.UserStationPageController;
import com.ecobike.components.vehicle.gui.UserVehicleListPane;

@SuppressWarnings("serial")
public class UserStationListPane extends ADataListPane<Station>{
	
	public UserStationListPane(ADataPageController<Station> controller) {
		this.controller = controller;
	}
	

	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {
		JButton button = new JButton("Xem xe trong b�i");
		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				System.out.println(singlePane.getData().getId_park());
//				new UserVehicleListInStationPane(singlePane.getData());
				
			}
		});
	}
}
