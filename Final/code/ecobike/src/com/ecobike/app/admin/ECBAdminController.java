package com.ecobike.app.admin;

import javax.swing.JPanel;

import com.ecobike.bean.Station;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.station.controller.AdminStationPageController;
import com.ecobike.components.vehicle.bike.controller.AdminBikePageController;

public class ECBAdminController {
	public ECBAdminController() {
	}
	
	public JPanel getStationPage() {
		ADataPageController<Station> controller = new AdminStationPageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getBikePage() {
		ADataPageController<Vehicle> controller = new AdminBikePageController();
		return controller.getDataPagePane();
	}
}