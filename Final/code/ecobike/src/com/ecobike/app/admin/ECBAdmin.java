package com.ecobike.app.admin;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class ECBAdmin extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public ECBAdmin(ECBAdminController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		

		JPanel stationPage = controller.getStationPage();
		tabbedPane.addTab("Bãi đỗ xe", null, stationPage, "Bãi đỗ xe");
		 
		JPanel bikePage = controller.getBikePage();
		tabbedPane.addTab("Xe đạp", null, bikePage, "Xe đạp");
		
		tabbedPane.addTab("Digital Video Discs", null, new JPanel(), "Digital Video Discs");
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Team 6");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ECBAdmin(new ECBAdminController());
			}
		});
	}
}