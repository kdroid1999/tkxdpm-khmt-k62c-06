package com.ecobike.app.user;

import javax.swing.JPanel;

import com.ecobike.bean.Station;
import com.ecobike.bean.Vehicle;
import com.ecobike.components.abstractdata.controller.ADataPageController;
import com.ecobike.components.order.controller.OrderController;
import com.ecobike.components.station.controller.UserStationPageController;
import com.ecobike.components.vehicle.bike.controller.UserBikePageController;
import com.ecobike.components.vehicle.eBike.controller.UserEBikePageController;
import com.ecobike.components.vehicle.twinBike.controller.UserTwinBikePageController;

public class ECBUserController {
	private OrderController orderController;
	
	public ECBUserController() {
		orderController = new OrderController();
	}
	
	public JPanel getOrderPane() {
		return orderController.getOrderPane();
	}
	
	public JPanel getStationPage() {
		ADataPageController<Station> controller = new UserStationPageController();
//		controller.setOrderController(orderController);
		return controller.getDataPagePane();
	}
	
	public JPanel getBikePage() {
		ADataPageController<Vehicle> controller = new UserBikePageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getTwinBikePage() {
		ADataPageController<Vehicle> controller = new UserTwinBikePageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getEBikePage() {
		ADataPageController<Vehicle> controller = new UserEBikePageController();
		return controller.getDataPagePane();
	}
	
	public JPanel getBikeInStationPage() {
		ADataPageController<Vehicle> controller = new UserBikePageController();
		return controller.getDataPagePane();
	}
}
