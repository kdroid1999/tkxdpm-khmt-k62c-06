package com.ecobike.app.user;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class ECBUser extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public ECBUser(ECBUserController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
		
		rootPanel.add(controller.getOrderPane(), BorderLayout.NORTH);
		
		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		
		JPanel stationPage = controller.getStationPage(); 
		tabbedPane.addTab("Bãi đỗ xe", null, stationPage, "Bãi đỗ xe");
		
		JPanel bikePage = controller.getBikePage();
		tabbedPane.addTab("Xe đạp", null, bikePage, "Xe đạp");
		
		JPanel twinBikePage = controller.getTwinBikePage();
		tabbedPane.addTab("Xe đạp đôi", null, twinBikePage, "Xe đạp đôi");
		
		JPanel eBikePage = controller.getEBikePage();
		tabbedPane.addTab("Xe đạp điện", null, eBikePage, "Xe đạp điện");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Team 6");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ECBUser(new ECBUserController());
			}
		});
	}
}