package com.ecobike.bean;

import com.ecobike.bean.Station;

public class Station {
    private int id_park;
	private String stationName;
    private String stationAddress;
    private int numberOfBikes;
    private int numberOfEBikes;
    private int numberOfTwinBikes;
    private int numberOfEmptyDocks;
    private float areaToWalk;
    private int timeToWalk;
    
    public Station() {
		super();
	}

    public Station(int id, String stationName, String stationAddress, int numberOfBikes, int numberOfEBikes, int numberOfTwinBikes, int numberOfEmptyDocks, float areaToWalk, int timeToWalk) {
        this.id_park = id;
        this.stationName = stationName;
        this.stationAddress = stationAddress;
        this.numberOfBikes = numberOfBikes;
        this.numberOfEBikes = numberOfEBikes;
        this.numberOfTwinBikes = numberOfTwinBikes;
        this.numberOfEmptyDocks = numberOfEmptyDocks;
        this.areaToWalk = areaToWalk;
        this.timeToWalk = timeToWalk;
    }
    
    public Station(int id, String name, String address) {
        this.id_park = id;
        this.stationName = name;
        this.stationAddress = address;
    }

	public int getId_park() {
		return id_park;
	}

	public void setId_park(int id_park) {
		this.id_park = id_park;
	}

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public int getNumberOfBikes() {
        return numberOfBikes;
    }
    
    public void setNumberOfBikes(int numberOfBikes) {
        this.numberOfBikes = numberOfBikes;
    }

    public int getNumberOfEBikes() {
        return numberOfEBikes;
    }

    public void setNumberOfEBikes(int numberOfEBikes) {
        this.numberOfEBikes = numberOfEBikes;
    }

    public int getNumberOfTwinBikes() {
        return numberOfTwinBikes;
    }

    public void setNumberOfTwinBikes(int numberOfTwinBikes) {
        this.numberOfTwinBikes = numberOfTwinBikes;
    }

    public int getNumberOfEmptyDocks() {
        return numberOfEmptyDocks;
    }

    public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
        this.numberOfEmptyDocks = numberOfEmptyDocks;
    }

	public float getAreaToWalk() {
		return areaToWalk;
	}

	public void setAreaToWalk(float areaToWalk) {
		this.areaToWalk = areaToWalk;
	}

	public int getTimeToWalk() {
		return timeToWalk;
	}

	public void setTimeToWalk(int timeToWalk) {
		this.timeToWalk = timeToWalk;
	}

	@Override
	public String toString() {
		return "id=" + this.id_park + ", stationName=" + this.stationName + ", stationAddress=" + this.stationAddress + ", numberOfBikes=" + this.numberOfBikes + ", numberOfEBikes="
				+ this.numberOfEBikes + ", numberOfTwinBikes=" + this.numberOfTwinBikes + ", areaToWalk=" + this.areaToWalk + ", timeToWalk="
				+ this.timeToWalk;
	}
	
	public boolean match(Station station) {
		if (station == null)
			return true;

		if(this.id_park==station.getId_park()) return true;

		if (station.stationAddress != null && !station.stationAddress.equals("") && !this.stationAddress.contains(station.stationAddress)) {
			return false;
		}

		if (station.stationName != null && !station.stationName.equals("") && !this.stationName.contains(station.stationName)) {
			return false;
		}

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Station) {
			return this.id_park == (((Station) obj).id_park);
		}
		return false;
	}
	
}
