package com.ecobike.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class EBike extends Vehicle {
	private int batteryPercentage;
	private int loadCycles;
	private int timeRemaining;

	public EBike() {
		super();
	}

	public EBike(int id_bike, String name, String type, float weight, String licensePlate,
			String manufacturingDate, String producer, float cost, int idPark) {
		super(id_bike, name, type, weight, licensePlate, manufacturingDate, producer, cost, idPark);
	}

	public EBike(int id_bike, String name, String type, float weight, String licensePlate,
			String manufacturingDate, String producer, float cost, int batteryPercentage, int loadCycles,
			int timeRemaining, int idPark) {

		super(id_bike, name, type, weight, licensePlate, manufacturingDate, producer, cost, idPark);
		this.batteryPercentage = batteryPercentage;
		this.loadCycles = loadCycles;
		this.timeRemaining = timeRemaining;
	}

	public int getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

	public int getLoadCycles() {
		return loadCycles;
	}

	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}

	public int getTimeRemaining() {
		return timeRemaining;
	}

	public void setTimeRemaining(int timeRemaining) {
		this.timeRemaining = timeRemaining;
	}

	@Override
	public String toString() {
		return super.toString() + "batteryPercentage=" + batteryPercentage + ", loadCycles=" + loadCycles
				+ ", timeRemaining=" + timeRemaining;
	}

}