package com.ecobike.bean.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ecobike.bean.Station;

public class SearchStationBlackBox {

private Station station = new Station(1, "Bach Khoa", "1 Dai Co Viet");
	
	@Test
	public void testMatchStation1() {
		Station station = new Station(1, "What", "Where");
		assertTrue("Station Match", this.station.match(station));
	}
	
	@Test
	public void testMatchStation2() {
		Station station = new Station(2, "Bach", "");
		assertTrue("Station Match", this.station.match(station));
	}
	
	@Test
	public void testMatchStation3() {
		Station station = new Station(2, "Xay Dung", "");
		assertTrue("Station Match", !this.station.match(station));
	}
	
	@Test
	public void testMatchStation4() {
		Station station = new Station(2, "Khoa", "2");
		assertTrue("Station Match", !this.station.match(station));
	}
	
	@Test
	public void testMatchStation5() {
		Station station = new Station(2, "Dung", "2");
		assertTrue("Station Match", !this.station.match(station));
	}

}
