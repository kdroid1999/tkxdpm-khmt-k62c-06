package com.ecobike.bean;

import java.util.ArrayList;
import java.util.Date;

public class TwinBike extends Vehicle {

	public TwinBike() {
		super();
	}

	public TwinBike(int id_bike, String name, String type, float weight, String licensePlate,
			String manufacturingDate, String producer, float cost, int idPark) {
		super(id_bike,name, type, weight, licensePlate, manufacturingDate, producer, cost, idPark);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}