package com.ecobike.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("vehicle")
@JsonSubTypes({ @Type(value = Bike.class, name = "bike"), @Type(value = TwinBike.class, name = "twinbike"),
		@Type(value = EBike.class, name = "ebike") })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicle {
	private int id_bike;
	private String name;
	private String type;
	private float weight;
	private String licensePlate;
	private String manuafacturingDate;
	private String producer;
	private float cost;
	private int idPark;

	public Vehicle() {
		super();
	}

	public Vehicle(int id_bike, String name, String type, float weight, String licensePlate, String manuafacturingDate,
			String producer, float cost, int idPark) {
		this.id_bike = id_bike;
		this.name = name;
		this.type = type;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.manuafacturingDate = manuafacturingDate;
		this.producer = producer;
		this.cost = cost;
		this.idPark = idPark;
	}
	
	public int getId_bike() {
		return id_bike;
	}

	public void setId_bike(int id_bike) {
		this.id_bike = id_bike;
	}

	public int getIdPark() {
		return idPark;
	}

	public void setIdPark(int idPark) {
		this.idPark = idPark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getManuafacturingDate() {
		return manuafacturingDate;
	}

	public void setManuafacturingDate(String manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vehicle) {
			return (this.id_bike == ((Vehicle) obj).id_bike);
		}
		return false;
	}

	@Override
	public String toString() {
		return "id=" + this.id_bike + ", name=" + this.name + ", type=" + this.type + ", weight=" + this.weight
				+ ", license_plate=" + this.licensePlate + ", manuafacturing_date=" + this.manuafacturingDate
				+ ", producer=" + this.producer + ", cost=" + this.cost;
	}

}