package com.ecobike.bean;

public class Order {
    private int id;
    private int cardId;
    private int bikeId;
    private int stationId;
    private int predictCost;
    private int realCost;
    private String timeRenting;

    public Order(int id, int cardId, int bikeId, int stationId, int predictCost, int realCost, String timeRenting) {
        this.id = id;
        this.cardId = cardId;
        this.bikeId = bikeId;
        this.stationId = stationId;
        this.predictCost = predictCost;
        this.realCost = realCost;
        this.timeRenting = timeRenting;
    }
    
    public Order() {
		super();
	}

    public Order(int vehicleId, String vehicleName, float vehicleCost) {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getBikeId() {
        return bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }
    
    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getPredictCost() {
        return predictCost;
    }

    public void setPredictCost(int predictCost) {
        this.predictCost = predictCost;
    }

    public int getRealCost() {
        return realCost;
    }

    public void setRealCost(int realCost) {
        this.realCost = realCost;
    }

    public String getTimeRenting() {
        return timeRenting;
    }

    public void setTimeRenting(String timeRenting) {
        this.timeRenting = timeRenting;
    }
}
