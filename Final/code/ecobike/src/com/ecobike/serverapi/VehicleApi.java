package com.ecobike.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.Station;
import com.ecobike.bean.TwinBike;
import com.ecobike.bean.Vehicle;
//import com.ecobike.bean.Station;

public class VehicleApi {
	public static final String PATH = "http://localhost:9999/";
	
	private Client client;
	
	public VehicleApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Station> getAllStation() {
		WebTarget webTarget = client.target(PATH).path("all_station");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>(){});
		System.out.println(res);
		return res;
	}

	public ArrayList<Station> getStations(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("all_station");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) { 
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		System.out.println(res);
		return res;
	}
	
	public ArrayList<Vehicle> getAllBikeInStation() {
		WebTarget webTarget = client.target(PATH).path("all_bike");
 
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>(){});
		System.out.println(res);
		return res;
	}

	public ArrayList<Vehicle> getBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("all_bike"); 
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>() {});
		System.out.println(res);
		return res;
	}
	

	public Bike updateBike(Bike bike) {
		WebTarget webTarget = client.target(PATH).path("update_bike");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));
		
		Bike res = response.readEntity(Bike.class);
		return res;
	}
	
	public void createVehicle(Vehicle vehicle) {
		WebTarget webTarget = client.target(PATH).path("add_bike");
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(vehicle, MediaType.APPLICATION_JSON));
		
		System.out.println(response);
	}
	
	public Station updateStation(Station station) {
		WebTarget webTarget = client.target(PATH).path("update_station");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
		
		Station res = response.readEntity(Station.class);
		System.out.println(res);
		return res;
	}
	
	public Station createStation(Station station) {
		WebTarget webTarget = client.target(PATH).path("add_station");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
		
		Station res = response.readEntity(Station.class);
		System.out.println(res);
		return res;
	}
	
	public ArrayList<Vehicle> getBikesInStation(Station station) {
		WebTarget webTarget = client.target(PATH).path("all_bike").path(station.getId_park() + ""); 
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>() {});
		System.out.println(res);
		return res;
	}


	public ArrayList<TwinBike> getTwinBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("all_bike").path("twinbike");

		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<TwinBike> res = response.readEntity(new GenericType<ArrayList<TwinBike>>() {});
		System.out.println(res);
		return res;
	}

	
	public ArrayList<EBike> getEBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("all_bike").path("ebike");

		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<EBike> res = response.readEntity(new GenericType<ArrayList<EBike>>() {});
		System.out.println(res);
		return res;
	}
}
